import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MyMaterialModule } from './material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoggedOutGuard } from './guards/logged-out.guard';    


export function initConfig(config: Config) {
  return () => config.load();
}

import { Config } from './services/config.service';
import { LoadingInterceptor } from './interceptors/loading.interceptor';
import { LoggedInGuard } from './guards/logged-in.guard';
import { NavbarComponent } from './components/dashboard/navbar/navbar.component';
import { BuscarPlatoComponent } from './components/modales/buscar-plato/buscar-plato.component';
import { ConfirmarEliminarComponent } from './components/modales/confirmar-eliminar/confirmar-eliminar.component';
import { LoggerInterceptor } from './interceptors/logger.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    NavbarComponent,
    BuscarPlatoComponent,
    ConfirmarEliminarComponent
  ],
  imports: [
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    MyMaterialModule,
    BrowserAnimationsModule
  ],
  providers: [
    [LoggedOutGuard],
    [LoggedInGuard],
    Config,
    {
      provide: APP_INITIALIZER,
      useFactory: initConfig,
      deps: [Config],
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoggerInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoadingInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
