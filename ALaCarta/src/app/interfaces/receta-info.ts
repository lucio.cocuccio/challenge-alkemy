export interface RecetaInfo {
    vegetarian: boolean,
    vegan: boolean,
    glutenFree: boolean,
    dairyFree: boolean,
    veryHealthy: boolean,
    cheap: boolean,
    veryPopular: boolean,
    sustainable: boolean,
    lowFodmap: boolean,
    weightWatcherSmartPoints: number,
    gaps: string,
    preparationMinutes: number,
    cookingMinutes: number,
    aggregateLikes: number,
    healthScore: number,
    pricePerServing: number,
    extendedIngredients: Array<any>,
    id: number,
    title: string,
    readyInMinutes: number,
    servings: number,
    sourceUrl: string,
    openLicense: number,
    image: string,
    imageType: string,
    summary: string,
    cuisines: Array<any>,
    dishTypes: Array<any>,
    diets: Array<any>
    occasions: Array<any>,
    winePairing: Object,
    instructions: string,
    analyzedInstructions: Array<any>,
    sourceName: null,
    creditsText: null,
    originalId: null,
    spoonacularSourceUrl: string
}
