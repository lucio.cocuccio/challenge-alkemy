export interface DataQuery {
    queryName: string,
    queryValue: string|boolean|number
}
