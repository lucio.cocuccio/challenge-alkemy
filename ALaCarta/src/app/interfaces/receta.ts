export interface Receta { 
    id: number,
    title: string,
    image: string,
    readyInMinutes: number,
    vegan: boolean,
    healthScore: number,
    pricePerServing: number,
    desc: string,
    servings: number
}
