import { Injectable } from '@angular/core';
import { DataQuery } from '../interfaces/data-query';
import { Receta } from '../interfaces/receta';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root'
})
export class RecetasService {

  private menu:Receta[] = []
  private contadorVegano:number = 0;
  private contadorNoVegano:number = 0;

  constructor(private rest: RestService
    ) { }

  setContadores(contV:number, contNV:number){
    this.contadorNoVegano += contNV;
    this.contadorVegano += contV;
  }

  getContadores(){
    let contadores = [this.contadorNoVegano, this.contadorVegano]
    return contadores; 
  }

  getRecetaBuscador(queryArray:DataQuery[]){
    return this.rest.getRecipeBuscador(queryArray)
  }
  
  getRecetaInfo(id:number, queryValue:boolean){    
    return this.rest.getRecipeDesc(id, queryValue);
  }

  getPlatos():Receta[]{
    return this.menu;
  }

  removePlato(plato:Receta){
    let indexOfObject = this.menu.findIndex(object => {
      return object.id === plato.id;
    });
    this.menu.splice(indexOfObject, 1);
    if (plato.vegan){
      this.contadorVegano -=1;
    } else {
      this.contadorNoVegano -=1;
    }
  }

  addPlato(plato:Receta){
    this.menu.push(plato)
  }
}
