import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, throwError } from 'rxjs';
import { DataQuery } from '../interfaces/data-query';
import { Config } from './config.service';
import { SnackbarService } from './snackbar.service';

@Injectable({
  providedIn: 'root'
})
export class RestService {
  login: string;
  preUrl: string;
  apiKey: string;

  constructor(
    private http: HttpClient,
    private config: Config,
    private snackBar: SnackbarService,
  ){
    this.login = this.config.getConfig('login');
    this.preUrl = this.config.getConfig('api');
    // this.apiKey = "4f789815e6904df1a22ff31a0620f22d" //correo i2t
    this.apiKey = "b6bdc970bc1048748f96321e35091fae" //correo personal
    // this.apiKey = "4ed97afa2044461ba77c48237771df76" //correo dos
    //  this.apiKey = "62a1d16eef8c4f6c93aea4bc3067a1bd" //hotmail tres

    
  }

  getRecipeBuscador(queryArray:DataQuery[]){
    let urlFinal = this.preUrl + "recipes/complexSearch";    
    let queryParams = new HttpParams()
      .append('apiKey', this.apiKey)
    queryArray.forEach(element => {
      queryParams = queryParams.append(element.queryName, element.queryValue);
    });  
    return this.http.get(urlFinal, {
      params: queryParams
    });
  }
    
  getRecipeDesc(id:number, queryValue:boolean){
    let urlFinal = this.preUrl + "recipes/"+id+"/information";
    const params = new HttpParams()
      .append('apiKey', this.apiKey)
      .append("includeNutrition", queryValue)
    return this.http.get(urlFinal, {
      params: params
    });
  }
  
  /** Caso especial para login(), no requiere token **/
  doLogin(body: string){
    const params = new HttpParams()
      .append('email',(JSON.parse(body)).email)
      .append('password',(JSON.parse(body)).password)
    return this.http.post(this.login,body, {
      params: params
    });
  }

  /** Wrapper para manejo de errores http y rcode **/
  callLogin(body: string, showSnack: boolean = true){
    let result = this.doLogin(body)
    .pipe(catchError( (e: any) => {
        this.snackBar.restException(e.error);
        this.snackBar.restError(e.error);        
        return throwError(() => new Error('RTxt: ' + e.error))
        
        // throwError({RTxt: e.message});
      }))
      .pipe(map(
        (data: any) => {
          if (data.token){
            // console.log("Data del login: ", data);
            return data;
          }
          else{
            if (showSnack)
              this.snackBar.restError(data);
            throw data;
          }
        }));

    return result;
  }
}
