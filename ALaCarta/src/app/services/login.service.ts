import { Injectable } from '@angular/core';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root'
})
/** Logueo del usuario, obtiene el token **/
export class LoginService {

  constructor(
    private rest: RestService
  ){
  }

  public login(body: string){
    /*
     'usuario'
     'pass'
    */
    return this.rest.callLogin(body);
  }

  public logout() :void {    
    localStorage.removeItem('TOKEN');    
    }    
}
