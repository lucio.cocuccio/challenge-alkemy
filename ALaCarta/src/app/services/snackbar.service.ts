import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class SnackbarService {

  toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  }) 

  constructor(){}
  
  showDesc(title:string, img:string, desc:string){
    Swal.fire({
      title: title,
      html: desc,
      imageUrl: img,
      imageWidth: 400,
      imageHeight: 200,
      imageAlt: 'Custom image',
    })
  }

  succes(msj:string){
    this.toast.fire({
      icon: 'success',
      title: msj
    })
  }

  error(msj:string){
    this.toast.fire({
      icon: 'error',
      title: msj
    })
  }

  /** Error de comunicacion (excepciones) contra la api rest **/
  restException(err: any){
    console.log('%c REST Error ', 'background: #f00; color: #fff', err.error);
  }

  /** Mensajes comunes **/
  message(msg: any){
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: msg,
    })
  }

  /** Errores esperables de la api rest **/
  restError(response: any){
    this.message(response.error);
    console.log('%c Fallo ', 'background: #b60; color: #fff', response.error);
  }
}
