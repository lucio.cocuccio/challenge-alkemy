import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { delay, finalize, tap } from 'rxjs/operators';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { LoadingComponent } from '../components/modales/loading/loading.component';

@Injectable()
export class LoadingInterceptor implements HttpInterceptor {

  private countRequest = 0;
  private dialogRef: any = null;
  constructor(
    private dialog: MatDialog
  ) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if(this.countRequest == 0)
      Promise.resolve(null).then( //evita ExpressionChangedAfterItHasBeenCheckedError
        () => this.dialogRef = this.dialog.open(LoadingComponent)
      );
    this.countRequest++;
    return next.handle(req).pipe(
      //delay(3000), //para testear
      finalize(() => {
        this.countRequest--;
        if(this.countRequest == 0){
          this.dialogRef.close();
          this.dialogRef = null;
        }
      })
    );
  }
}
