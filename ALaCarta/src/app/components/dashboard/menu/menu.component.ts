import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { RecetasService } from 'src/app/services/recetas.service';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { BuscarPlatoComponent } from '../../modales/buscar-plato/buscar-plato.component';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {  

  promedioPreparacion?:number;
  precioTotal:number = 0;
  healthScoreTotal?:number;

  constructor(private recetaService:RecetasService, private dialog:MatDialog, private _snackBarService:SnackbarService) { }

  ngOnInit(): void {
  }  

  ngDoCheck(){
    this.setPromedios()
  }

  setPromedios(){
    let preparacionAux:number = 0;
    let precioAux:number = 0;
    let healthAux:number = 0;
    this.recetaService.getPlatos().forEach(element => {
      preparacionAux += element.readyInMinutes;
      precioAux += element.pricePerServing;
      healthAux += element.healthScore;
    });
    this.promedioPreparacion = +((preparacionAux/ this.recetaService.getPlatos().length).toFixed(2));
    this.precioTotal = +((precioAux).toFixed(2));
    this.healthScoreTotal = +((healthAux/ this.recetaService.getPlatos().length).toFixed(2));
  }


  openModalSearch(){
    if(this.recetaService.getPlatos().length<4){
      // console.log(this.recetaService.getPlatos().length)
      this.dialog.open(BuscarPlatoComponent,{
        // height:'350px'
      }).afterClosed().subscribe((rta) =>{
        // console.log(rta)
        this.setPromedios()
      })  
    } else {
      this._snackBarService.error('El Menu no puede contener mas de 4 platos');
    }
    
  }
}
