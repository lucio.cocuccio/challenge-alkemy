import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlatoTresComponent } from './plato-tres.component';

describe('PlatoTresComponent', () => {
  let component: PlatoTresComponent;
  let fixture: ComponentFixture<PlatoTresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlatoTresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlatoTresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
