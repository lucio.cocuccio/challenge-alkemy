import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlatoUnoComponent } from './plato-uno.component';

describe('PlatoUnoComponent', () => {
  let component: PlatoUnoComponent;
  let fixture: ComponentFixture<PlatoUnoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlatoUnoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlatoUnoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
