import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlatoDosComponent } from './plato-dos.component';

describe('PlatoDosComponent', () => {
  let component: PlatoDosComponent;
  let fixture: ComponentFixture<PlatoDosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlatoDosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlatoDosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
