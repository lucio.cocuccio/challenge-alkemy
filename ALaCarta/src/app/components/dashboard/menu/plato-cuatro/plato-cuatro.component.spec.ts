import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlatoCuatroComponent } from './plato-cuatro.component';

describe('PlatoCuatroComponent', () => {
  let component: PlatoCuatroComponent;
  let fixture: ComponentFixture<PlatoCuatroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlatoCuatroComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlatoCuatroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
