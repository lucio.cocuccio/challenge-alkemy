import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmarEliminarComponent } from 'src/app/components/modales/confirmar-eliminar/confirmar-eliminar.component';
import { Receta } from 'src/app/interfaces/receta';
import { RecetasService } from 'src/app/services/recetas.service';
import { SnackbarService } from 'src/app/services/snackbar.service';

@Component({
  selector: 'app-plato-cuatro',
  templateUrl: './plato-cuatro.component.html',
  styleUrls: ['./plato-cuatro.component.css']
})
export class PlatoCuatroComponent implements OnInit {

  detail!:Receta;
  menu!:Receta[];

  constructor(private recetaService:RecetasService, public dialog:MatDialog, private _snackBarService:SnackbarService) { }

  ngOnInit(): void {    
    this.menu = this.recetaService.getPlatos();
    // console.log("es este",this.menu)
  }

  confirmarEliminar(plato:Receta){
    this.dialog.open(ConfirmarEliminarComponent, {
      width: '420px',
      panelClass: 'confirm-dialog-container',
      disableClose: true,     
    }).afterClosed().subscribe((rta) =>{
      if (rta){
        this.recetaService.removePlato(plato);
        this.menu = this.recetaService.getPlatos();        
      }      
    })
  }   

  mostrarDescripcion(title:string, img:string, desc:string){
    this._snackBarService.showDesc(title,img, desc);
  }
}
