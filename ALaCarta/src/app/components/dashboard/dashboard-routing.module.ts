import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoggedOutGuard } from 'src/app/guards/logged-out.guard';
import { DashboardComponent } from './dashboard.component';
import { MenuComponent } from './menu/menu.component';

const routes: Routes = [
 { path : '', component:DashboardComponent, canActivate : [LoggedOutGuard], children: [
    {path: 'menu', component: MenuComponent },
  ]},
  { path : '', component:DashboardComponent, canActivate : [LoggedOutGuard], children: [
    {path: 'menu', component: MenuComponent },
  ]},
  { path : '', component:DashboardComponent, canActivate : [LoggedOutGuard], children: [
    {path: 'menu', component: MenuComponent },
  ]},
  { path : '', component:DashboardComponent, canActivate : [LoggedOutGuard], children: [
    {path: 'menu', component: MenuComponent },
  ]},
  { path : '', component:DashboardComponent, canActivate : [LoggedOutGuard], children: [
    {path: 'menu', component: MenuComponent },
  ]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
