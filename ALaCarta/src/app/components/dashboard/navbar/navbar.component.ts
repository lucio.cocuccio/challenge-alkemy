import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';
import { MenuService } from 'src/app/services/menu.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  menu: any[] = [];
  public isMenuOpen: boolean = false;

  constructor(private _menuService: MenuService, private loginService: LoginService) { }
  
  ngOnInit(): void {
    this.cargarMenu();
  }

  cargarMenu(){
    this._menuService.getMenu().subscribe(data => {      
      this.menu = data;
    })
  }

  logOut(){
    this.loginService.logout()
  }

  onSidenavClick(): void {
    this.isMenuOpen = false;
  }
}
