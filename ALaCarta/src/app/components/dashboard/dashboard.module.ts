import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { MyMaterialModule } from 'src/app/material';
import { MenuComponent } from './menu/menu.component';
import { PlatoUnoComponent } from './menu/plato-uno/plato-uno.component';
import { PlatoDosComponent } from './menu/plato-dos/plato-dos.component';
import { PlatoTresComponent } from './menu/plato-tres/plato-tres.component';
import { PlatoCuatroComponent } from './menu/plato-cuatro/plato-cuatro.component';

@NgModule({
  declarations: [      
  
    MenuComponent, PlatoUnoComponent, PlatoDosComponent, PlatoTresComponent, PlatoCuatroComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    MyMaterialModule
  ]
})
export class DashboardModule { }
