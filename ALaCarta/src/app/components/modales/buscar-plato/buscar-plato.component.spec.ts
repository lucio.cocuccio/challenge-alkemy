import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuscarPlatoComponent } from './buscar-plato.component';

describe('BuscarPlatoComponent', () => {
  let component: BuscarPlatoComponent;
  let fixture: ComponentFixture<BuscarPlatoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuscarPlatoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuscarPlatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
