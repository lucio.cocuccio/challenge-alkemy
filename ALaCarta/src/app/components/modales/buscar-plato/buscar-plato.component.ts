import { Component, OnInit } from '@angular/core';
import { DataQuery } from 'src/app/interfaces/data-query';
import { Receta } from 'src/app/interfaces/receta';
import { ListasService } from 'src/app/services/listas.service';
import { RecetasService } from 'src/app/services/recetas.service';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';


@Component({
  selector: 'app-buscar-plato',
  templateUrl: './buscar-plato.component.html',
  styleUrls: ['./buscar-plato.component.css']
})
export class BuscarPlatoComponent implements OnInit {
  
  value = '';
  rtaSearch:any;

  cuisineArray: any[]  = []
  selectedCuisine: string = '';
  dietaArray: any[] = []; 
  selectedDiet:string = '';
  intolerancesArray: any[] = [];
  selectedIntolerance:string = '';
  mealTypeArray:any[] = [];
  selectedType:string = '';
  queryValues: DataQuery[] = [];

  private debounceTimer?: NodeJS.Timeout;

  selectedPlato?: Receta;
  invalidaBoton:boolean = true;  
  private contadorNoVegano!:number;
  private contadorVegano!:number;

  cols!: number;

  gridByBreakpoint = {
    xl: 3,
    lg: 3,
    md: 3,
    sm: 2,
    xs: 2
  }

  constructor(private breakpointObserver: BreakpointObserver, private _listasService:ListasService, private recetaService:RecetasService, private _snackBarService:SnackbarService) { 
    this.breakpointObserver.observe([
      Breakpoints.XSmall,
      Breakpoints.Small,
      Breakpoints.Medium,
      Breakpoints.Large,
      Breakpoints.XLarge,
    ]).subscribe(result => {
      if (result.matches) {
        if (result.breakpoints[Breakpoints.XSmall]) {
          this.cols = this.gridByBreakpoint.xs;
        }
        if (result.breakpoints[Breakpoints.Small]) {
          this.cols = this.gridByBreakpoint.sm;
        }
        if (result.breakpoints[Breakpoints.Medium]) {
          this.cols = this.gridByBreakpoint.md;
        }
        if (result.breakpoints[Breakpoints.Large]) {
          this.cols = this.gridByBreakpoint.lg;
        }
        if (result.breakpoints[Breakpoints.XLarge]) {
          this.cols = this.gridByBreakpoint.xl;
        }
      }
    });
  }

  ngOnInit(): void {
    this.cargarListas();
    this.cargarContadores();
  }
  
  cargarContadores(){
    this.contadorNoVegano = this.recetaService.getContadores()[0];
    this.contadorVegano = this.recetaService.getContadores()[1];
  }

  cargarListas(){
    this._listasService.getLista("mealType").subscribe((data:any) => {      
      this.mealTypeArray = data;
    });
    this._listasService.getLista("cuisine").subscribe((data:any) => {      
      this.cuisineArray = data;
    });
    this._listasService.getLista("diets").subscribe((data:any) => {      
      this.dietaArray = data;
    });
    this._listasService.getLista("intolerances").subscribe((data:any) => {      
      this.intolerancesArray = data;
    });
  }

  onQueryChanged(query: string = ''){
    if ( this.debounceTimer) clearTimeout(this.debounceTimer);
    if (query.length>2){
    this.debounceTimer = setTimeout(() => {
      // console.log(query)
      this.buscar(query);
    }, 1000)
    }
  }

  selectPlato(id:any){
    this.recetaService.getRecetaInfo(id,false).subscribe((data:any)=>{
      // console.log(data)
      let plato = {
        id : data.id,
        title : data.title,
        readyInMinutes : data.readyInMinutes,
        image : data.image,
        desc : data.summary,
        vegan : data.vegan,
        healthScore : data.healthScore,
        pricePerServing : data.pricePerServing,
        servings: data.servings 
      };      
      this.selectedPlato = plato;
      this.invalidaBoton = false;
      });
  }

  agregarPlato(){        
    if (this.selectedPlato){
      if (this.selectedPlato.vegan){
        if (this.contadorVegano<2){
          this.recetaService.addPlato(this.selectedPlato);
          // console.log("plato",this.selectedPlato);
          this.recetaService.setContadores(1,0)
        } else {
          this._snackBarService.error('No puede agregar mas de 2 platos veganos')
        }
      } else {
        if (this.contadorNoVegano<2){
          this.recetaService.addPlato(this.selectedPlato);
          // console.log("plato",this.selectedPlato);
          this.recetaService.setContadores(0,1)
        } else {
          this._snackBarService.error('No puede agregar mas de 2 platos que no sean veganos')
        }
      } 
    }

    this.cargarContadores();
    // console.log("contador vegano", this.contadorVegano)
    // console.log("contador No vegano", this.contadorNoVegano)

  }

  buscar(query:string){
    this.queryValues = [];
    if (this.selectedCuisine!=''){
      this.queryValues.push({queryName:'cuisine', queryValue:this.selectedCuisine})
    }
    if (this.selectedDiet!=''){
      this.queryValues.push({queryName:'diet', queryValue:this.selectedDiet})
    }
    if (this.selectedIntolerance!=''){
      this.queryValues.push({queryName:'intolerances', queryValue:this.selectedIntolerance})
    }
    if (this.selectedType!=''){
      this.queryValues.push({queryName:'type', queryValue:this.selectedType})
    }
    if (query!='') {
      this.queryValues.push({queryName:'query', queryValue:query})
    }
    this.recetaService.getRecetaBuscador(this.queryValues).subscribe((rta:any)=>{
      // console.log(rta)
      if (rta.results.length==0){
        this._snackBarService.error('No existen coincidencias con su busqueda')
      } else {
        this.rtaSearch = rta.results
      }
    })
  }
}
