import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-confirmar-eliminar',
  templateUrl: './confirmar-eliminar.component.html',
  styleUrls: ['./confirmar-eliminar.component.css']
})
export class ConfirmarEliminarComponent implements OnInit {

  constructor(private matDialogRef:MatDialogRef<any>) { }

  ngOnInit(): void {
  }

  aceptar(){
    this.matDialogRef.close(true);    
  }
}

