import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
import { SnackbarService } from 'src/app/services/snackbar.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
/** Logueo del usuario
 * Redirige al menu y guarda variables en el localStorage
  **/
 export class LoginComponent implements OnInit {
  formulario: FormGroup = new FormGroup({
    'usuario': new FormControl('', Validators.required),
    'password': new FormControl('', Validators.required)
  });

  constructor(
    private loginService: LoginService,
    private router: Router,
    private _snackBarService: SnackbarService
  ) {}

  ngOnInit() {
    sessionStorage.clear();
    localStorage.clear();
  }

  login(){
    let jsonbody = JSON.stringify({
      'email': this.formulario.controls['usuario'].value,
      'password': this.formulario.controls['password'].value
    });

    /** Autentica al usuario y guarda credenciales en el localStorage **/
    this.loginService.login(jsonbody).subscribe({
      next: (v) => {
        let token = v.token;  
        localStorage.setItem('TOKEN', token);
      },
      error: (e) => {
        console.error(e);
      },
      complete: () => {
        this._snackBarService.succes('Signed in successfully');        
        this.router.navigate(['/dashboard'])
      }       
    })
  }
}