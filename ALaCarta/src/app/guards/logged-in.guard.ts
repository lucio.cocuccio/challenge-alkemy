import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoggedInGuard implements CanActivate {
  constructor(private router: Router) { }      
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if (this.isLoggedIn()) {      
        return true;      
      }              
  this.router.navigate(['/dashboard']);      
  return false;      
  }      

  public isLoggedIn(): boolean {      
    let status = false;      
    if (!localStorage.hasOwnProperty("TOKEN")) {      
       status = true;      
    } else {      
      status = false;      
    }      
    return status;      
  } 
}  